const fs = require('fs');

const fileName = './test.txt';

try{
  fs.writeFileSync(fileName, 'Hello, something!');
  console.log('File was saved');
} catch (e) {
  console.error(e);
}
