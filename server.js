const express = require('express');
const app = express();
const fileDb = require('./fileDb');
fileDb.init();

app.use(express.json());

const port = 9000;

const messages = require('./app/messages');

app.use('/messages', messages);

app.listen(port, () => {
  console.log(`Server started on ${port} port`);
});